class role::minecraft_server {
    include profile::base
    include profile::ssh_server
    include profile::minecraft
}
