class profile::ssh_server {
    $public_key = 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC9EpGN4qqVQ9uc3q3pbBpX13ytlET56xbkwK41/3+WWCLFgylb+hwJ5VpJ2aM6Dgh18IqcacEW2EiWtFgRD5XXO+izRONT+jyr/p6RmC6WNOMSqPM9U4O0x5/VIBmvyBLB0kYoMexwdtk6UtOHqa/omoax26Qnjp/LzYGRx7o81QI7ZFYgN6cT3WLAVf/2/1HRlO5FC4A15/xy4pNRIn0BaKfaXK7a+T5F/u/Mcn7iYtn9v4kByzKOm38D/9GasPjE0Ov4aBVAAPP2x86/8EF9xpgiIcSzxKIlIHYILIdYQjGCmB/0cvzRCVD7BMTzkWID5PSmecgjwI3R/84rZ1gF'
    package { 'openssh-server':
        ensure => present,
    }
    service { 'sshd':
        ensure => running,
        enable => true,
    }
    ssh_authorized_key { 'root@master.puppet.vm':
        ensure  => present,
        user    => 'root',
        type    => 'ssh-rsa',
        key     => $public_key,
    }

}
