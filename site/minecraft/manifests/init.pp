class minecraft (
    $server_url = 'https://piston-data.mojang.com/v1/objects/f69c284232d7c7580bd89a5a4931c3581eae1378/server.jar',
    $install_dir = '/opt/minecrafting'
){
    package { 'jdk-18':
        provider => 'rpm',
        ensure => present,
        source => 'https://download.oracle.com/java/18/latest/jdk-18_linux-x64_bin.rpm',
        before => File['/opt/minecraft/server.jar'],
    }
    file { $install_dir:
        ensure => directory,
    }
    file { "${install_dir}/server.jar":
        ensure => file,
        source => $server_url,
        before => Service['minecraft'],
    }
    file { "${install_dir}/eula.txt":
        ensure => file,
        content => 'eula=true',
    }
    file { '/etc/systemd/system/minecraft.service':
        ensure => file,
    #    source => 'puppet:///modules/minecraft/minecraft.service',
        content => epp('minecraft/minecraft.service', {
            install_dir => $install_dir,
        })
    }
    service { 'minecraft':
        ensure => running,
        enable => true,
        require => [Package['jdk-18'],File["${install_dir}/server.jar"],File["${install_dir}/eula.txt"],File['/etc/systemd/system/minecraft.service']],
    }
}
